package dev.fr13.gitlabdeploydemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitlabDeployDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(GitlabDeployDemoApplication.class, args);
	}

}
